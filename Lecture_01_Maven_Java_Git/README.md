# Неделя 1. Задание 1

Интерфейс IGreeting позволяет узнать информацию о человеке

1. Напишите класс GreetingImpl, который реализует интерфейс IGreeting.
   Реализуйте метод toString указав значащие поля

2. Метод getHobbies возвращает список. Вам необходимо реализовать класс Hobby, getters & toString

Final-поля класса Hobby:
- id: String
- name: String
- description: String

Конструкторы класса Hobby:

ctor(String id, String name, String description);
ctor(String id, String name);



# Критерии приемки
1. Класс GreetingImpl должен содержать информацию о вас. Класс Hobby должен быть реализован согласно требованиям
2. Класс GreentingImpl должен быть протестирован с помощью JUnit на предмет возвращаемых значений.
3. Сделать PR из ветви feature/solution вашего репозитория в репозиторий с заданием
